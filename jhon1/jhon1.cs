﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Robocode;
using System.IO;
using System.Security.Permissions;
using System.Drawing;
using Robocode.Util;
using System.Numerics;

namespace nvl.jhon1
{
    public class jhon1 : Robot
    {
        int _ESCAPE_FROM_BULLET_AHEAD_OR_BACK = 0;
        bool _ACTION_START = false;
        bool _IS_TARGET = false;

        public override void Run()
        {


            while (true)
            {
                if (_ACTION_START == false)
                {
                    TurnGunRight(40);
                    Ahead(25);
                }
                else
                {
                    TurnRadarRight(15);
                    Scan();
                }
            }

            //base.Run();
        }

        public override void OnScannedRobot(ScannedRobotEvent e)
        {
            Stop(true);
            double offset = 0;

            if (e.Bearing < 0)
            {
                offset = 360 + e.Bearing + Heading;
            }
            else
            {
                offset = e.Bearing + Heading;
            }


            SetGunForHit(offset);



            if (e.Distance < 400)
            {
                Fire(Rules.MAX_BULLET_POWER);
            }
            else
            {
                Fire(1);
            }

        }


        public override void OnBulletMissed(BulletMissedEvent e)
        {

            _IS_TARGET = false;

        }


        public override void OnBulletHit(BulletHitEvent e)
        {
            if (!_ACTION_START)
            {
                _ACTION_START = true;
                SetTankForCorner(e.Bullet.Heading);
            }

            CustomLog("JA TRAFIŁEM. power trafienia: " + e.Bullet.Power.ToString("F2") + " heat: " + GetGunHeat().ToString("F2") + " moja enerdia: " + Energy.ToString("F2") + Environment.NewLine);


            double gunFactor = e.Bullet.Heading - GunHeading;
            if ((gunFactor > -1 && gunFactor < 1) && _IS_TARGET)
            {
                if (GetGunHeat() == 0)
                    Fire(Rules.MAX_BULLET_POWER);
            }
            else
                SetGunForHit(e.Bullet.Heading);
        }


        private void SetTankForCorner(double bulletHeading)
        {
            double offsetL = -this.Heading + bulletHeading;

            while (Math.Abs(offsetL) > 360)
                if (offsetL < 0)
                    offsetL += 360;
                else
                    offsetL -= 360;

            if (offsetL < 0)
                offsetL = 360 + offsetL;

            if ((offsetL >= 0 && offsetL < 2) || (offsetL > 358 && offsetL <= 360))
            {
                return;
            }

            if (offsetL >= 0 && offsetL < 180)
                TurnRight(offsetL - 90);
            else
                TurnLeft(-offsetL - 90);


        }

        private void SetGunForHit(double bulletHeading)
        {
            double offsetL = -this.GunHeading + bulletHeading;

            while (Math.Abs(offsetL) > 360)
                if (offsetL < 0)
                    offsetL += 360;
                else
                    offsetL -= 360;

            if (offsetL < 0)
                offsetL = 360 + offsetL;

            if ((offsetL >= 0 && offsetL < 2) || (offsetL > 358 && offsetL <= 360))
            {
                _IS_TARGET = true;
                return;
            }

            if (offsetL >= 0 && offsetL < 180)
                TurnGunRight(offsetL);
            else
                TurnGunLeft(360 - offsetL);


            _IS_TARGET = true;
        }

        public override void OnHitByBullet(HitByBulletEvent e)
        {
            if (!_ACTION_START)
            {
                _ACTION_START = true;
                SetTankForCorner(e.Heading + 180);
            }

            CustomLog("TRAFILI MNIE. bearing: " + e.Bearing.ToString("F2") + " heading: " + e.Heading.ToString("F2")
                + " bearing radian: " + e.BearingRadians.ToString("F2") + " heading radian: " + e.HeadingRadians.ToString("F2"));



            SetGunForHit(e.Heading + 180);
            Fire(1);

            if (_ESCAPE_FROM_BULLET_AHEAD_OR_BACK == 0)
            {
                Back(200);
                _ESCAPE_FROM_BULLET_AHEAD_OR_BACK = 1;
            }
            else
            {
                Ahead(200);
                _ESCAPE_FROM_BULLET_AHEAD_OR_BACK = 0;
            }

            _IS_TARGET = false;
            Scan();

        }

        public override void OnHitWall(HitWallEvent e)
        {
            _IS_TARGET = false;

            if ((e.Bearing >= -90 && e.Bearing <= 0) || (e.Bearing >= 0 && e.Bearing <= 90))
                Back(200);
            else
                Ahead(200);

            TurnRight(15);
        }

        #region tools
        private double GetGunHeat()
        {
            return this.GunHeat;
        }

        private void CustomLog(string msg)
        {


            try
            {

            }
            catch (Exception)
            {

            }
        }
        #endregion


    }
}
